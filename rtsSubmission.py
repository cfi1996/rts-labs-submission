from typing import List, Counter
from collections import Counter

class Submission:
    def aboveBelow(self, ints: List[int], target: int): #-> Counter[int] # return type commented for compat with Python < 3.9
        cntr = Counter({
            "above": 0,
            "below": 0
        })
        # not checking type/composition of ints, since that would require additonal iter thru the whole loop
        if type(target) is int and ints:
            for i in ints:
                if i > target:
                    cntr["above"] += 1
                elif i < target:
                    cntr["below"] += 1
        return cntr

    def stringRotation(self, input: str, amount: int) -> str:
        if type(amount) is not int or not input or type(input) is not str or amount < 0: 
            return None #return None for invalid input
        elif amount == 0 or amount == len(input):
            return input #no rotation necessary 
        elif amount > len(input):
            amount = amount % len(input) # rotate only by relevant amount.
        return input[-amount:len(input)] + input[0:len(input)-amount] # basic rotate


def main():
    ints =  [1, 5, 2, 1, 10]
    print(Submission().aboveBelow(ints, 6))
    print(Submission().stringRotation("MyString",2))

if __name__ == "__main__":
    main()
